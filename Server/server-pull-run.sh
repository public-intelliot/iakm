#!/bin/bash
COMPOSE_FILE="docker-compose-pull.yml"
grep -v '^#' server.env
export $(grep -v '^#' server.env | xargs)

# Create network---------------------------------------------------------------------------------------------------------------------
sudo docker network create --opt com.docker.network.bridge.name=iakm  --driver=bridge --subnet=${SUBNET}  iakm-public-network
# Login to "iotplatform.eurecom.fr" private regisry----------------------------------------------------------------------------------
sudo docker login iotplatform.eurecom.fr:5000
# PULL and RUN server network services----------------------------------------------------------------------------------------------- 
sudo docker-compose -f $COMPOSE_FILE --env-file server.env up 
