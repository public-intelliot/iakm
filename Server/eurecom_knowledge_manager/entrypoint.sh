#!/bin/sh


# Better to not run the worker server without ID
if [ -z "${WORKER_ID}" ]; then
  echo "[-] You need to set the WORKER_ID environment variable"
  exit
fi
# echo "Starting Knowledge Manager of EDGE server"
exec python knowledge_manager.py

