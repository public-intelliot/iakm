import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
import socketio
import json
import socket
import os
import ssl
from _thread import *
from threading import Thread
import threading
import time
from datetime import datetime
import pickle
import numpy as np
import pymongo, gridfs
from gridfs import GridFS
from bson import ObjectId
import torch
import sys
import traceback
from torch import nn
from torch import optim
from flask import Flask, render_template, request
from flask_socketio import SocketIO
from flask_socketio import send, emit
from dataclasses import dataclass
import logging
MQTT_KEEPALIVE_INTERVAL = 60
MONGO_MAX_SIZE = 16000000 # mongo supports BSON document sizes up to 16793598 bytes.

env_mosquitto = os.environ['IP_MOSQUITTO']
env_mongo = os.environ['IP_MONGO']
print(f"Mongo IP:{env_mongo}", flush=True)
print(f"Mosquitto IP: {env_mosquitto}", flush=True)

#mos_HOST,mos_PORT =  "eurecom-mosquitto", 8883      #"192.168.12.208"
mos_HOST,mos_PORT =  env_mosquitto, 8883     
mos_USERNAME, mos_PASSWORD = "nadar", "Ali@1234"

#mongo_HOST ,mongo_PORT = "eurecom-mongo", "27017"
mongo_HOST ,mongo_PORT = env_mongo, "27017"

hostname=socket.gethostname()   
my_HOST, my_PORT=socket.gethostbyname(hostname), 8183
print(f"My IP: {my_HOST}", flush=True)

server_FL = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_FL.bind((my_HOST,my_PORT)) 
server_FL.listen(5)

conn = None
Customers = []

#region "Classes & Functions"
class Semantic:
    models = []
    topics = []
    merged_model = None
    def __init__(self, topic):
        self.IDs = []
        self.models = []
        self.topics = []
        self.merged_model = None
        topic_array = np.array(topic.split('/'))
        self.entity = topic_array[6]
        if self.entity == "roundabout":
            self.properties = {"nbr_lanes":topic_array[7], "nbr_exits": topic_array[8], "radius": topic_array[9] }
        elif self.entity == "forest":
            self.properties = {"lighting":topic_array[7], "model_type": topic_array[8] }
        else:
            i, j= 7, 1
            self.properties = {}
            while len(topic_array) > i: 
                self.properties[f"key{j}"] = topic_array[i]
                i +=1
                j +=1
    
    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def isMatching(self, agentData)-> bool:
        if agentData.entity != self.entity: return False
        if agentData.entity == 'roundabout':
            if agentData.properties != self.properties: return False
        if agentData.entity == 'forest':
            if agentData.properties != self.properties: return False
        return True

class AgentData:
    def __init__(self, topic, model=None):
        self.topic = topic
        self.model = model
        topic_array = np.array(topic.split('/'))
        self.method = topic_array[0]
        self.model_usage = topic_array[1]
        self.agent_type = topic_array[2]
        self.trainability = topic_array[3]
        self.agent_id = topic_array[4]
        self.edge_id = topic_array[5]
        self.entity = topic_array[6]
        if self.entity == "roundabout":
            self.properties = {"nbr_lanes":topic_array[7], "nbr_exits": topic_array[8], "radius": topic_array[9] }
        elif self.entity == "forest":
            self.properties = {"lighting":topic_array[7], "model_type": topic_array[8] }
        else:
            i, j= 7, 1
            self.properties = {}
            while len(topic_array) > i: 
                self.properties[f"key{j}"] = topic_array[i]
                i +=1
                j +=1

@dataclass
class AgentModel:
    topic: str
    method: str
    model_usage: str
    agent_type: str
    trainability: bool
    agent_id: str
    edge_id: str
    entity: str
    properties: {}
    model: bytes
    matches : []
    SimilarAvailableAgents : []
    modelSize:int
    def __init__(self, topic , model:bytes = None):
        self.topic = topic
        topic_array = np.array(topic.split('/'))
        self.method = topic_array[0]
        self.model_usage = topic_array[1]
        self.agent_type = topic_array[2]
        self.trainability = topic_array[3]
        self.agent_id = topic_array[4]
        self.edge_id = topic_array[5]
        self.entity = topic_array[6]
        if self.entity == "roundabout":
            self.properties = {"nbr_lanes":topic_array[7], "nbr_exits": topic_array[8], "radius": topic_array[9] }
        elif self.entity == "forest":
            self.properties = {"lighting":topic_array[7], "model_type": topic_array[8] }
        else:
            i, j= 7, 1
            self.properties = {}
            while len(topic_array) > i: 
                self.properties[f"key{j}"] = topic_array[i]
                i +=1
                j +=1
        self.model = model
        self.modelSize = 0
        self.matches = []
        self.SimilarAvailableAgents = []
    
    def get_binary_model(self):
        mongo_client = pymongo.MongoClient(f'mongodb://{mongo_HOST}:{mongo_PORT}/')
        mydb = mongo_client['data']
        mycollection = mydb['models']
        myquery = {"entity": self.entity, **self.properties}
        mydata = mycollection.find(myquery)
        print(f"QUERY: {myquery}")
        mydata = list(mydata)
        if len(mydata) > 0:
            data = mydata[0]
            try:is_big = bool(data['is_big'])
            except: is_big = False
            if is_big:
                fs = gridfs.GridFS(mydb)
                big_id = data['big_id']
                print(f"BIG-ID: {big_id}")
                obj_model = fs.get(ObjectId(big_id)).read() 
                self.model = obj_model
            else:
                self.model = data['model']
    
    def getSimilarAgents(self):
        mongo_client = pymongo.MongoClient(f'mongodb://{mongo_HOST}:{mongo_PORT}/')
        mycollection = mongo_client['data']['agents']
        myquery = { "entity": self.entity, **self.properties}
        data = mycollection.find(myquery)
        data = list(data)
        if len(data) > 0:
            for _agent in data:
                self.SimilarAvailableAgents.append(_agent)
    
    def save_model(self):
        mongo_client = pymongo.MongoClient(f'mongodb://{mongo_HOST}:{mongo_PORT}/')
        mydb = mongo_client['data']
        mycollection = mydb['models']
        model_size = sys.getsizeof(self.model)
        if model_size > MONGO_MAX_SIZE:
            fs = gridfs.GridFS(mydb)
            big_id = fs.put(self.model)
            insert_data = {"is_big":1, "big_id":big_id,"entity": self.entity, **self.properties, "created_time": datetime.now()}
            insert = mycollection.insert_one(insert_data)
            _id = insert.inserted_id
            print(f"      ✓✓✓✓✓ Pushed 'BIG' Model has been saved successfully in Database - id: {_id}", flush=True )
        else:
            insert_data = {"is_big":0, "model":self.model,"entity": self.entity, **self.properties, "created_time": datetime.now()}
            insert = mycollection.insert_one(insert_data)
            _id = insert.inserted_id
            print(f"      ✓✓✓✓✓ Pushed 'SMALL' Model has been saved successfully in Database - id: {_id}", flush=True )




def getTopicAfterChangeAt(index: int, new_value: str, topic: str)-> str:
    topic_array = np.array(topic.split('/'))
    topic_array[index] = new_value
    new_topic = "/".join(topic_array)
    return new_topic

def save_merged_model(semantic: Semantic):
    print(semantic.IDs, flush=True)
    print(semantic.entity, flush=True)
    print(semantic.properties, flush=True)
    print(semantic.merged_model, flush=True)
    mongo_client = pymongo.MongoClient(f'mongodb://{mongo_HOST}:{mongo_PORT}/')
    mydb = mongo_client['data']
    mycollection = mydb['models']
    insert_data = {"model":semantic.merged_model, "merging_worker":len(semantic.IDs), "merging_time":datetime.now(),"entity":semantic.entity, **semantic.properties}
    print(insert_data, flush=True)
    try:
        insert = mycollection.insert_one(insert_data)
        _id, _ack = insert.inserted_id, insert.acknowledged
    except:
        print(sys.exc_info(), flush=True)
    print(f"      ✓✓✓✓✓ MERGED Model has been saved successfully in Database - id: {_id}", flush=True )

def topic_well_formated(topic):
    length_topic = len(topic.split('/'))
    if length_topic < 4:
        return False
    else:
        return True

def save_available_agent(agent):
    mongo_client = pymongo.MongoClient(f'mongodb://{mongo_HOST}:{mongo_PORT}/')
    mydb = mongo_client['data']
    mycollection = mydb['agents']

    myquery = {"topic": agent.topic,"_id": agent.agent_id, "entity":agent.entity,**agent.properties}
    newvalues = {"$set": {**myquery , "created_time": datetime.now()}}
    mycollection.update_one(myquery, newvalues, upsert = True)
    print('available agent has been saved successfully in Database !', flush=True )

#endregion

def task_FL_connection():
    global conn
    while True:
        conn, addr = server_FL.accept()
        pickled_semantic_data = conn.recv(40960)
        if pickled_semantic_data:
            print(">>>>> KM received Merged Pickled model from FL controller", flush=True)
            semantic_data = pickle.loads(pickled_semantic_data)
            save_merged_model(semantic_data)

            CustomerFilter = [customer for customer in Customers if semantic_data.isMatching(customer)]
            for customer in CustomerFilter:
                mqttc.publish(customer.topic, semantic_data.merged_model)
                print(f"      ✓✓✓✓✓ MERGED model is published to the Customer: {str(customer.agent_id)}" , flush = True)
                Customers.remove(customer)
            print(f"Lenght of Customers is {len(Customers)}")
            
            for i in range(len(semantic_data.IDs)):
                new_topic = getTopicAfterChangeAt(0, "pub-merge", semantic_data.topics[i])
                mqttc.publish(new_topic, semantic_data.merged_model)
                print(f"      ✓✓✓✓✓ MERGED model is published to the Agent: {str(semantic_data.IDs[i])}" , flush = True)

        time.sleep(0.1)
Thread(target=task_FL_connection, args=()).start()

def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT Broker!", flush=True)
    mqttc.subscribe("$SYS/broker/log/M/subscribe", 0)
    mqttc.subscribe("sub-available/available/#", 0)
    mqttc.subscribe("pub-model/train/#", 0)
    mqttc.subscribe("pub-model/push/#", 0)

def on_message(mqttc, obj, msg ):
    global Customers
    if msg.topic == "$SYS/broker/log/M/subscribe": # handle subscription
            if "$SYS/broker/log/M/subscribe" in str(msg.payload.decode("utf-8", "ignore")):
                print("-----------------------------------------------------------------------------------", flush=True)
                print("---> SERVER-KM subscribing to BROKER SYS logs to listen to AGENTS subs <---", flush=True)
                print("-----------------------------------------------------------------------------------", flush=True)
            else: # Some-Agent subscribe for Some-Service
                payload_array = json.loads(json.dumps(str(msg.payload.decode("utf-8", "ignore")))).split()
                _topic = payload_array[len(payload_array) -1]
                if topic_well_formated(_topic): # correct topic format
                    agent = AgentModel(_topic)

                    if agent.method == "sub-model":
                        print(f"Agent ({agent.agent_id}) is subscribing with topic: {_topic}" , flush=True)
                        if agent.model_usage == "use":
                            print("the agent " + agent.agent_id +" want a model to use.")
                            print("Loading model from Database...", flush=True)
                            agent.get_binary_model() 
                            if agent.model is None:
                                print("----- END: No model (for use) -----", flush=True)
                            else :
                                print("Publishing Model(use only) to: " + _topic, flush=True)
                                mqttc.publish(_topic, agent.model) 

                        if agent.model_usage == "train" and agent.trainability == "yes":
                            print(f"Agent ({agent.agent_id}) needs model [Trainability: {agent.trainability}]")
                            agent.get_binary_model()
                            if agent.model is None:
                                print("------- Model Not Found (for use) -----", flush=True)
                            else:
                                print("Loading Trainers from Database...", flush=True)
                                print("---> Publishing Initial Model to Original Trainer with topic: "+ agent.topic, flush=True)
                                mqttc.publish(agent.topic, agent.model) 
                                
                                agent.getSimilarAgents()
                                if len(agent.SimilarAvailableAgents) ==0:
                                    print("No Similar-Available-Agent in Database", flush=True)
                                else:
                                    print(f"There is -{len(agent.SimilarAvailableAgents)}- Similar trainer in Database", flush=True)
                                    new_topic = getTopicAfterChangeAt(0,"pub-model", agent.topic)
                                    #new_topic = agent.getTopicAfterChangeMethod()
                                    print(f"Subscribing to Trained Model with topic: {new_topic}" , flush=True)
                                    mqttc.subscribe(new_topic)  
                                    # Publish DB model to all other available agents && Subscribe to Trained model
                                    for similarAvailableAgent in agent.SimilarAvailableAgents:
                                        print("Publishing Initial Model(train and use) to the agent with topic: "+ similarAvailableAgent["topic"], flush=True)
                                        mqttc.publish(similarAvailableAgent["topic"], agent.model) 
                                        new_topic =  similarAvailableAgent["topic"].replace("sub-model","pub-model")
                                        print(f"Subscribing to Trained Model with topic: {new_topic}" , flush=True)
                                        #mqttc.subscribe(new_topic)  # replace "sub-model" method by "pub-model"
                         
                        if agent.model_usage == "train" and agent.trainability != "yes":
                            print(f"Ai-Customer ({agent.agent_id}) needs Merged model [Trainability: {agent.trainability}]")
                            agent.get_binary_model()
                            if agent.model is None:
                                print("------- Model Not Found in DB -----", flush=True)
                            else:
                                print("---- Loading Trainers from Database ---", flush=True)
                                agent.getSimilarAgents()

                                if len(agent.SimilarAvailableAgents) ==0:
                                    print("There is -NO- Similar available agent in Database", flush=True)
                                else:
                                    print(f"There is -{len(agent.SimilarAvailableAgents)}- Similar trainers in Database", flush=True)
                                    Customers.append(agent)
                                    # Publish DB model to all other available agents && Subscribe to Trained model
                                    for similarAvailableAgent in agent.SimilarAvailableAgents:
                                        print("Publishing Initial Model(train and use) to the agent with topic: "+ similarAvailableAgent["topic"], flush=True)
                                        mqttc.publish(similarAvailableAgent["topic"], agent.model) 
                                        new_topic =  similarAvailableAgent["topic"].replace("sub-model","pub-model")
                                        print(f"Subscribing to Trained Model with topic: {new_topic}" , flush=True)
                                        mqttc.subscribe(new_topic)  # replace "sub-model" method by "pub-model"
                    
                    if agent.method == "sub-available":
                        print(f"-----> Agent: ({agent.agent_id}) is available !", flush=True)
                        save_available_agent(agent)
    
    elif msg.topic.startswith('pub-model/push'): # Receiving model from Agent
        topic = msg.topic
        model = msg.payload
        agentModel = AgentModel(topic,model)
        size_temp = sys.getsizeof(model)
        agentModel.modelSize += size_temp
        print(f"Push Model received:", flush=True)
        print(f"---> Topic: {topic}", flush=True)
        print(f"---> Size : {size_temp} bytes (total: {agentModel.modelSize} bytes)", flush=True)
        agentModel.save_model()
        print(f"   --> Model Save in DB ", flush=True)
    
    
    elif msg.topic.startswith('pub-model'): # Receiving trained models from Agents
        topic = msg.topic
        model = msg.payload
        agentData = AgentData(topic,model)
        print(f">>> Trained Model received: Topic ({topic})", flush=True)
        pickled_agentData = pickle.dumps(agentData)
        conn.sendall(pickled_agentData)
        print(f"    >>> Send Trained Model to FL controller", flush=True)

  
try:
    mqttc = mqtt.Client("client-logger") 
    logger = logging.getLogger(__name__)
    mqttc.enable_logger(logger)
    mqttc.on_message = on_message

    mqttc.on_connect = on_connect
    print(f"MQTT host: {mos_HOST}", flush=True)
    print(f"MQTT port: {mos_PORT}", flush=True)
    mqttc.connect(host=mos_HOST, port=mos_PORT, keepalive=MQTT_KEEPALIVE_INTERVAL) 
    mqttc.loop_forever()
except Exception:
    print(traceback.format_exc())
