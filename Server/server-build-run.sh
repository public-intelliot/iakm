#!/bin/bash
COMPOSE_FILE="docker-compose-build.yml"
grep -v '^#' server.env
export $(grep -v '^#' server.env | xargs)

# Create network---------------------------------------------------------------------------------------------------------------------
sudo docker network create --opt com.docker.network.bridge.name=iakm  --driver=bridge --subnet=${SUBNET}  iakm-public-network
#PULL and BUILD server network services 
docker-compose -f $COMPOSE_FILE --env-file server.env pull iakm_server_db   # Pulling Mongo image     
docker-compose -f $COMPOSE_FILE --env-file server.env pull iakm_server_mqtt # Pulling Mosquitto image 
docker-compose -f $COMPOSE_FILE --env-file server.env build iakm_server_km  # Building KM container   
docker-compose -f $COMPOSE_FILE --env-file server.env build iakm_server_ai  # Building FML container  

#Run server network services 
docker-compose -f $COMPOSE_FILE --env-file server.env  up 

