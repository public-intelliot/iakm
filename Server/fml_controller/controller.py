import os
import json
import threading
from threading import Thread
import ssl
import time
import traceback
from datetime import datetime
import requests
import socketio
import socket
import pickle
import numpy as np
import pymongo
import torch
import sys
from torch import nn
from torch import optim
import syft as sy
from syft.frameworks.torch.fl import utils
from syft.federated.floptimizer import Optims
import net
from net import Net
from dataclasses import dataclass
from flask_socketio import send, emit
import init_model

#KM_HOST = "eurecom-knowledge-manager"
KM_HOST = os.environ['IP_KM']
KM_PORT = 8183

client_FL = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_FL.connect((KM_HOST,KM_PORT))
print(f"Connected to KM server @IP: {KM_HOST} ", end='', flush=True)

# my_HOST = "fml-controller"
# my_PORT = 8182
# server_FL = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# server_FL.bind((my_HOST,my_PORT)) 
# server_FL.listen(5)

conn = None
SEMANTICS = []

class AgentData:
    def __init__(self, topic, model=None):
        self.topic = topic
        self.model = model
        topic_array = np.array(topic.split('/'))
        self.method = topic_array[0]
        self.model_usage = topic_array[1]
        self.agent_type = topic_array[2]
        self.trainability = topic_array[3]
        self.agent_id = topic_array[4]
        self.edge_id = topic_array[5]
        self.entity = topic_array[6]
        if self.entity == "roundabout":
            self.properties = {"nbr_lanes":topic_array[7], "nbr_exits": topic_array[8], "radius": topic_array[9] }
        elif self.entity == "forest":
            self.properties = {"lighting":topic_array[7], "model_type": topic_array[8] }
        else:
            i, j= 7, 1
            self.properties = {}
            while len(topic_array) > i: 
                self.properties[f"key{j}"] = topic_array[i]
                i +=1
                j +=1

class Semantic:
    models = []
    topics = []
    merged_model = None
    def __init__(self, topic):
        self.IDs = []
        self.models = []
        self.topics = []
        self.merged_model = None
        topic_array = np.array(topic.split('/'))
        self.entity = topic_array[6]
        if self.entity == "roundabout":
            self.properties = {"nbr_lanes":topic_array[7], "nbr_exits": topic_array[8], "radius": topic_array[9] }
        elif self.entity == "forest":
            self.properties = {"lighting":topic_array[7], "model_type": topic_array[8] }
        else:
            i, j= 7, 1
            self.properties = {}
            while len(topic_array) > i: 
                self.properties[f"key{j}"] = topic_array[i]
                i +=1
                j +=1
    
    def __str__(self):
        return str(self.__class__) + ": " + str(self.__dict__)

    def isMatching(self, agentData)-> bool:
        if agentData.entity != self.entity: return False
        if agentData.entity == 'roundabout':
            if agentData.properties != self.properties: return False
        if agentData.entity == 'forest':
            if agentData.properties != self.properties: return False
        return True

def mergeThenSendToKM(semantic):
    global SEMANTICS
    models = {}
    for i in range(len(semantic.models)):
        models[semantic.IDs[i]] = pickle.loads(semantic.models[i])
    print(f"-----  the number of models to merge is :{str(len(semantic.models))} -----", flush=True)
    if len(models) == 0:
        print("No trained Models in the dictionary !!!", flush=True)
        return
    try:
        merged_model = utils.federated_avg(models)
    except Exception as e:
        print(traceback.print_exc())

    semantic.merged_model = pickle.dumps(merged_model)
    pickled_semantic_data = pickle.dumps(semantic)
    client_FL.sendall(pickled_semantic_data)
    print("------- Models are Merged -- AND -- Sent to KM ------------", flush=True)
    SEMANTICS.remove(semantic)
    del semantic
    models.clear()

init_model.add_initial_model() # add an AI model to DB for a given context

while True:
    data = client_FL.recv(40960)
    if not data: break
    agentData = pickle.loads(data)
    ID = agentData.agent_id
    topic = agentData.topic
    model = agentData.model
    if model:
        print(">>>>>>>> Controller received a model ",flush=True)
        SemanticsFiltered = [semantic for semantic in SEMANTICS if semantic.isMatching(agentData)]

        if len(SemanticsFiltered) == 0:
            new_semantic = Semantic(topic)
            new_semantic.models.append(model)
            new_semantic.topics.append(topic)
            new_semantic.IDs.append(ID)
            SEMANTICS.append(new_semantic)
            print("          >>>>> Start NEW thread to merge trained models",flush=True)
            threading.Timer(5,mergeThenSendToKM ,args=(new_semantic,)).start()
        else:
            print("          >>>>> Adding trained models to Thread",flush=True)
            exist_semantic = SemanticsFiltered[0]
            exist_semantic.models.append(model)
            exist_semantic.topics.append(topic)
            exist_semantic.IDs.append(ID)
