import json
import time
import os
import pickle
import torch
from torch import nn
from torch import optim
import pymongo
import time
from datetime import datetime
import net
from net import Net

mongo_HOST = os.environ['IP_MONGO'] #"eurecom-mongo" 
mongo_PORT = "27017"

data = torch.tensor([[0,0],[0,1],[1,0],[1,1.]])
target = torch.tensor([[0],[0],[1],[1.]])


def train(model):
    opt = optim.SGD(params=model.parameters(),lr=0.1)
    for iter in range(10):
        # 1) erase previous gradients (if they exist)
        opt.zero_grad()
        # 2) make a prediction
        pred = model(data)
        # 3) calculate how much we missed
        loss = ((pred - target)**2).sum()
        # 4) figure out which weights caused us to miss
        loss.backward()
        # 5) change those weights
        opt.step()
        # 6) print our progress
        #print("Loss progress:" + str(loss.data))
    return model

def add_initial_model():
    model_initial = Net()
    model_trained = train(model_initial)
    pickled_model = pickle.dumps(model_trained)

    myclient = pymongo.MongoClient(f'mongodb://{mongo_HOST}:{mongo_PORT}/')
    mydb = myclient['data']
    mycollection = mydb['models']
    insert_data = {"model":pickled_model,"nbr_lanes":'2', "seg_type":'roundabout', "nbr_exits": '4', "radius": '5', "created_time": datetime.now()}
    insert = mycollection.insert_one(insert_data)
    _id, _ack = insert.inserted_id, insert.acknowledged
    #myquery = {"nbr_lanes":'2', "seg_type": 'roundabout', **{"nbr_exits": '4',"radius": '5'}}
    #newvalues = {"$set": {"model": model, "created_time": datetime.now(), **myquery }}
    #mycollection.update_one(myquery, newvalues, upsert = True)
    #print('New Model has been saved successfully in Database :-)', flush=True )


def save_model(pickled_model, properties):
    myclient = pymongo.MongoClient(f'mongodb://{mongo_HOST}:{mongo_PORT}/')
    mydb = myclient['data']
    mycollection = mydb['models']
    insert_data = {"model":pickled_model,**properties, "created_time": datetime.now()}
    insert = mycollection.insert_one(insert_data)
    _id, _ack = insert.inserted_id, insert.acknowledged
    return _ack
  


if __name__ == '__main__':
    # test1.py executed as script
    # do something
    add_model()

