#!/bin/bash

grep -v '^#' ai.env
export $(grep -v '^#' ai.env | xargs)

docker container stop ${AI_HOST} 
docker container rm ${AI_HOST} 
docker image rm ${AI_HOST} 




