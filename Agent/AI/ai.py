import os
import sys
import traceback
import requests
from struct import pack
import pickle
import argparse
import time
import logging
import json
import socket
import threading
import torch
from torch import nn, optim
from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
from werkzeug.serving import make_server
data = torch.tensor([[0,0],[0,1],[1,0],[1,1.]])
target = torch.tensor([[0],[0],[1],[1.]])
app = Flask(__name__)
api = Api(app)
############################################################################
AI_HOST, AI_PORT =socket.gethostbyname(socket.gethostname()) , os.environ['AI_PORT']
IAKM_HOST, IAKM_PORT = os.environ['IAKM_HOST'], os.environ['IAKM_PORT']
URL_IAKM_base = "http://"+ IAKM_HOST+":"+IAKM_PORT 


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(2, 1)

    def forward(self, x):
        x = self.fc1(x)
        return x

def topicToSemantic(topic):
    array = topic.split('/')
    semantic = {}
    semantic['method'] = array[0]
    semantic['model_usage'] = array[1]
    semantic['agent_type'] = array[2]
    semantic['trainability'] = array[3]
    semantic['agent_id'] = array[4]
    semantic['edge_id'] = array[5]
    semantic['entity'] = array[6]
    i=7
    j=1
    while len(array) > i: 
        semantic[f"key{j}"] = array[i]
        i +=1
        j +=1
    return semantic
    
def train(m):
    opt = optim.SGD(params=m.parameters(),lr=0.1)
    for iter in range(10):
        # 1) erase previous gradients (if they exist)
        opt.zero_grad()
        # 2) make a prediction
        pred = m(data)
        # 3) calculate how much we missed
        loss = ((pred - target)**2).sum()
        # 4) figure out which weights caused us to miss
        loss.backward()
        # 5) change those weights
        opt.step()
        # 6) print our progress
        print("Loss progress: " + str(loss.data))
    return m

class ResourceTrain(Resource):
    def post(self):
        data_pickled_model = request.get_data(parse_form_data=True)
        topic = request.args.get('topic')
        semantic = topicToSemantic(topic)
        model = pickle.loads(data_pickled_model) # decode AI model
        trained_model = train(model) # train AI model
        pickled_trained_model = pickle.dumps(trained_model) # re-encode AI-model 
        params_pub = semantic
        params_pub['method'] = 'pub-model' 
        print(" ----- Send trained model to Edge Server ----- ", flush=True)
        # Request merged model by attaching trained AI-model
        msgMergedModel = requests.post(f"{URL_IAKM_base}/getMergedModel", data=pickled_trained_model, params=params_pub)
        merged_AI_model = msgMergedModel.content
        print("----- Received (merged) model from Geoserver ------", flush=True)
        print(str(merged_AI_model), flush=True)
        #model = pickle.loads(merged_AI_model)
        print("----- END -----", flush=True)
api.add_resource(ResourceTrain, '/train')

class ServerThread(threading.Thread):
    def __init__(self, app):
        threading.Thread.__init__(self)
        self.server = make_server(AI_HOST, AI_PORT, app)
        self.ctx = app.app_context()
        self.ctx.push()
    def run(self):
        print('starting AI API server........')
        self.server.serve_forever()

json_request = {}

def getModel():
    req = requests.get(f"{URL_IAKM_base}/model", params=json_request)
    status = req.status_code
    try:
        content = req.content.decode()
        if type(content) == str:
            print(f"-- no model --", flush=True)
            return
    except: content = req.content
    if status ==200:
        size_content = sys.getsizeof(content)
        print(f"Status: {status} ", flush=True)
        print(f"Received Model Size : {size_content} bytes", flush=True)       
        print(f"Model: {content} ", flush=True)     
    else:
        print(json.dumps(content,indent=1), flush=True)
      
def pushModel_big(): 
    dataSeimensModelAsByte = getSeimensModel()
    #the_data = {"context": json_request, "model": dataSeimensModelAsByte}
    #headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    req = requests.post(f"{URL_IAKM_base}/model",data=dataSeimensModelAsByte,params=json_request)
    response = {"code": req.status_code, "content": req.content.decode()}
    print(response, flush=True)
def getSeimensModel()-> bytes:
    data_read = None
    with open("./workpieceStorage_marker_model_small.hdf5", 'rb') as readFile:
        data_read  = readFile.read()
    pickled = pickle.dumps(data_read)
    print(f"✓✓✓ SIEMENS Model Pickled; Size : {sys.getsizeof(pickled)} bytes", flush=True)
    return pickled
def pushModel_small(): 
    model_initial = Net()
    model_trained = train(model_initial)
    dataSmallModel = pickle.dumps(model_trained)
    req = requests.post(f"{URL_IAKM_base}/model",data=dataSmallModel,params=json_request)
    response = {"code": req.status_code, "content": req.content.decode()}
    print(response, flush=True)

def sendAvailability():
    json_request['_ai_host'] = str(AI_HOST)
    json_request['_ai_port'] = str(AI_PORT)
    message = requests.post(f"{URL_IAKM_base}/availability", params=json_request, stream=True)
    content = message.content.decode()
    if message.status_code == 202: 
        try:
            print('Availability Accepted!', flush=True)
            global server
            server = ServerThread(app)
            server.start()
        except Exception:
            print(traceback.format_exc())
    else:                          
        print(json.dumps(content,indent=1), flush=True)

def getTrainedModel():
    getModel = requests.get(f"{URL_IAKM_base}/train", params=json_request)
    data = getModel.content
    print("Receiving AI model <bytes>", flush=True)
    print("Local-AI Starts Training...", flush=True)
    model = pickle.loads(data)
    trained_model = train(model)
    pickled = pickle.dumps(trained_model)
    print("Local-AI Ends Training...", flush=True)
    params_pub = json_request
    params_pub['method'] = 'pub-model'
    print(" ---> Publishing trained AI model", flush=True)
    reqMergedModel = requests.post(f"{URL_IAKM_base}/getMergedModel", data=pickled,params=params_pub)
    status = reqMergedModel.status_code
    if status == 200:
        content = reqMergedModel.content
        print("----- Received (merged) model from Geoserver ------", flush=True)
        print(str(content), flush=True)
        #model = pickle.loads(merged_AI_model)
        print("----- END -----", flush=True)
    else:
        print('Something went wrong!', flush=True)


if __name__ == "__main__":
    method = ""
    usage = ""
    uc = os.environ['MODEL_USAGE']
    
    if uc == "uc-use":
        method = "sub-model"
        usage = "use"

    elif uc == "uc-push":
        method = "pub-model"
        usage = "push"

    elif uc == "uc-available":
        method = "sub-available"
        usage = "available"

    elif uc == "uc-train":
        method = "sub-model"
        usage = "train"
    else:
        exit()


    json_request = {
    "method": method,
    "model_usage": usage,
    "agent_type":os.environ['AGENT_TYPE'],
    "trainability": os.environ['TRAINABILITY'],
    "agent_id": os.environ['AGENT_ID'],
    "edge_id": os.environ['EDGE_ID']
    }
    

    try:
        f = open('context.json')
        context = json.load(f)
        json_request["entity"] = context["entity"]
        for key,value in context['attributes'].items():
            json_request[key] = value
    except:
        print(sys.exc_info(), flush=True)
        exit()



    if uc == "uc-use":         getModel()
    if uc == "uc-push":        pushModel_small()
    elif uc == "uc-available": sendAvailability() 
    elif uc == "uc-train":     getTrainedModel() 













# docker exec -it aiaas_server_mongo_1 /bin/sh
# mongo -> show databases -> use data
# db.getCollection('models').find()
