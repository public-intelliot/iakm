#!/bin/bash
grep -v '^#' ai.env
export $(grep -v '^#' ai.env | xargs)

docker container stop ${AI_HOST} 
docker container rm ${AI_HOST} 
docker image rm ${AI_HOST} 

sudo docker login iotplatform.eurecom.fr:5000

sudo docker image pull iotplatform.eurecom.fr:5000/intellio/agent-ai:latest

sudo docker run --name ${AI_HOST}  --net="host" --env-file ai.env iotplatform.eurecom.fr:5000/intellio/agent-ai:latest
