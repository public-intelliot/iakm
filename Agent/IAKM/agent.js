/*
* Copyright 2019-2020 EURECOM
* Author - Nadar Ali (ali.nadar@eurecom.fr)
* Date created - 11/10/2021
*/
'use strict'
const express = require("express");
const app = express();

// Constants
const PORT = process.env.AGENT_PORT;
const HOST = '0.0.0.0';
const BROKER_HOST = process.env.BROKER_HOST;
const BROKER_PORT = process.env.BROKER_PORT;
const req_timeinterval = 10
const req_timeout = req_timeinterval * 12000

var fileupload = require("express-fileupload");
var fs = require('fs');
var path = require('path')
var mqtt = require("mqtt");
var request = require('request');
var sizeof = require('object-sizeof')
const server = require('http').createServer(app)
var bodyParser = require("body-parser"); // for parsing the body in POST request
var morgan = require('morgan');
const router = express.Router();
global.received={};
global.AI_micorservices=[];


app.use(bodyParser.json({limit: '200mb',extended: true}));
app.use(bodyParser.urlencoded({ limit: '200mb',extended: true }));
app.use(bodyParser.json({
    limit: '250mb'
  })); 
  app.use(bodyParser.urlencoded({
    limit: '250mb',
    parameterLimit: 100000000,
    extended: true 
  }));
app.use(express.static('public'))
app.use(morgan('dev'));// use morgan to log requests to the console
app.use(fileupload());

const mqtt_options = { port: BROKER_PORT, host: BROKER_HOST, clientId: process.env.AGENT_HOST};
const mqttclient = mqtt.connect(mqtt_options);
mqttclient.on("connect",function(     ){
    console.log("Connected to Geoserver Broker! ");
});
mqttclient.on("error"  ,function(error){
    console.log("Cannot connect to Mosquitto !!! " + error);
});
mqttclient.on('message',function(topic, message, packet){
	 console.log("Received message with topic: "+ topic); 
	 if (topic.includes('sub-available/available'))
	 {
        var ai_id = topic.split("/")[4]
        var filtered_micorservices = global.AI_micorservices.filter(item => item.ai_id == ai_id)
        if (filtered_micorservices.length > 0){
            var ai_host = filtered_micorservices[0]['ai_host']
            var ai_port = filtered_micorservices[0]['ai_port']
            // EDGE assigns training model to Agent
            request.post({
                url:'http://'+ ai_host +':'+ ai_port +'/train?topic='+topic,
                body: message,
                encoding: null},
                function(error, response, body){
                    if (error == null)console.log("Model-Train has been assigned ! ");
                    else console.log(error);
            });
          }
     }
     else
     {
        global.received[topic] = message;
     }
  });

var message_badRequest = {
"status": "Bad Request",
"message": "IAKM agent cannot understand your request",
"accepted-format-example":{
    "method": "sub-model|push-model",
    "model_usage": "use|push|train|available",
    "agent_type":"vehicle|customer",
    "trainability": "yes|no",
    "agent_id": "your_agent_id",
    "edge_id": "edge_id",
    "your_context_keys": "your_context_values"
}
}
function validBody(dict){
    var valid = true
    if (Object.keys(dict).length < 6)valid = false
    else{
        var values = Object.values(dict)
        var list_methods = ['sub-model', 'pub-model','sub-available', 'push-model'];
        var list_usages = ['use', 'push', 'train','available'];
        var list_agentTypes = ['vehicle','customer'];
        var list_trainbability = ['yes','no'];
        if (!list_methods.includes(values[0])) valid = false
        if (!list_usages.includes(values[1]))valid = false
        if (!list_agentTypes.includes(values[2]))valid = false
        if (!list_trainbability.includes(values[3]))valid = false
    }
    return valid
}


// PUSH AI model
app.post("/model", function (req,res){
    var context = req.query
    if (!validBody(context))
        return res.status(400).end(JSON.stringify(message_badRequest));
    else
    {
        var topic_pub_push = Object.values(context).join('/')
        console.log("Agent is Pushing an AI model with topic :" + topic_pub_push)
        
        const msgs = []
        req.on('data', (chunk) => {
            if(chunk)msgs.push(chunk)
        })
        req.on('end', () => {
            const buffer_model = Buffer.concat(msgs)
            console.log("Model Received {size: " + sizeof(buffer_model) + " bytes}")
            mqttclient.publish(topic_pub_push, buffer_model)
            res.status(200).end('End Push-OK!');
        })
    }
});

// GET AI model
router.get("/model", async function(req, res) {
    var context = req.query
    if (!validBody(context))
        return res.status(400).end(JSON.stringify(message_badRequest));
    else
    {
        var topic_sub = Object.values(context).join('/')
        console.log("Subscribing to: "+ topic_sub);
        mqttclient.subscribe(topic_sub);
        var count_interval = 0
        const interval = setInterval(() =>
        {
            if (global.received[topic_sub])
            {
                clearInterval(interval);
                var model = global.received[topic_sub]
                delete global.received[topic_sub]
                mqttclient.unsubscribe(topic_sub);
                return res.status(200).end(model, 'binary');
            }
            if (count_interval > req_timeout)
            {
                clearInterval(interval);
                mqttclient.unsubscribe(topic_sub);
                return res.status(200).end('');
            }
            count_interval += req_timeinterval
        }, req_timeinterval);
    }
});

// Announce Agent Avaialability to train 
router.post("/availability", async function(req, res) {
    var context = req.query
    if (!validBody(context))
        return res.status(400).end(JSON.stringify(message_badRequest));
    else
    {   
        global.AI_micorservices.push(
            {'ai_id':context["agent_id"],
             'ai_host':context["_ai_host"],
             'ai_port':context["_ai_port"],
            })
        delete context["_ai_host"]
        delete context["_ai_port"]
        var array_context = Object.values(context)
        var topic_available = array_context.join('/')
        console.log("Subscribing to: " + topic_available)
        mqttclient.subscribe(topic_available);
        return res.status(202).json({"message":"Accepted"})
    }
});
router.delete("/availability", async function(req, res) {
    context = req.query
    agent_id = context["agent_id"]
    return res.status(202).json({"message":"Accepted"})
  });
  

// GET a model and Train it and ask for Merging 
router.get("/train", async function(req, res) {
    var context = req.query
    if (!validBody(context))
        return res.status(400).end(JSON.stringify(message_badRequest));
    else
    {
        var topic_sub = Object.values(context).join('/')
        console.log("Subscribing to: "+ topic_sub);
        mqttclient.subscribe(topic_sub);
        var count_interval = 0
        const interval = setInterval(() =>
        {
            if (global.received[topic_sub])
            {
                clearInterval(interval);
                var model = global.received[topic_sub]
                delete global.received[topic_sub]
                mqttclient.unsubscribe(topic_sub);
                return res.status(200).end(model, 'binary');
            }
            if (count_interval > req_timeout)
            {
                clearInterval(interval);
                mqttclient.unsubscribe(topic_sub);
                return res.status(200).end('');
            }
            count_interval += req_timeinterval
        }, req_timeinterval);
    }
});

router.post("/getMergedModel", function (req,res){
    var context = req.query
    var array_context = Object.values(context)
    var topic_pub_model = array_context.join('/')
    array_context[0] = "pub-merge"
    var topic_pub_merge = array_context.join('/')

    // subscribe for merging "sub-merge"
    console.log("Subscribing to:" + topic_pub_merge)
    mqttclient.subscribe(topic_pub_merge);

    req.on('data', (data) => {
        // publish trained model "pub-model"
        console.log("Publishing to:" + topic_pub_model)
        mqttclient.publish(topic_pub_model, data)
        const interval = setInterval(() =>{
            if (global.received[topic_pub_merge])
            {
                clearInterval(interval);
                var merged_model = global.received[topic_pub_merge]
                delete global.received[topic_pub_merge]
                return res.status(200).end(merged_model, 'binary');
            }
        }, 3);
	});
});

app.use('/', router);
//server.listen(PORT, () => {console.log(`Running on http://${HOST}:${PORT}`);});
app.listen(PORT, () => { console.log(`Running on http://${HOST}:${PORT}`);});


