

# IAKM agent endpoints  (single container)

Note: Before start building our IAKM docker container, you have to setup your .env file:
```
AGENT_IAKM=agent_iakm_1                    # given name for IAKM-Agent  
IAKM_BROKER_IP=192.168.10.133              # IP of IAKM's MQTT broker
```

## 1. Build & Run Agent Knowledge Management


Simply start the following shell script:

```bash
$ ./start-agent.sh
```

It will build a docker image, then create and run the container:
```bash
#!/bin/bash
###################################### EXPORT .ENV file ##################################################
grep -v '^#' iakm.env
export $(grep -v '^#' iakm.env | xargs)

###################################### BUILD IAKM agent ##################################################
docker build -t ${AGENT_IAKM} . 

####################################### RUN IAKM agent ###################################################
docker run --name ${AGENT_IAKM}  --net="host" --env-file iakm.env ${AGENT_IAKM}

```


## 2. Endpoints

### 2.1. Swagger Overview 

![Diagram](images/endpoints-swagger.png)

### 2.2. Swagger Code
<p>
<details>
<summary> Click here for <strong>Agent API Swagger Code</strong></summary>
<pre><code>
swagger: '2.0'
info:
  description: This is a experimental edition for IAKM agent API. Using this interface, user could send a request to the geoserver asking to set/get an AI-model for a given context. User could also ask for a trained model ((Federated Learning mechanism)) or announce his availability to train in a given context. For more information [https://gitlab.eurecom.fr/a-team/iakm]
  version: 1.0.0
  title: Knowledge Management 1.0
  termsOfService: https://gitlab.eurecom.fr/a-team/iakm
  contact:
    email: ali.nadar@eurecom.fr
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
tags:
  - name: AI-model
    description: Everything about how to set/get an AI model
  - name: Agents
    description: Announce/Remove availability to train AI-model
  - name: Federated Learning
    description: Everything about how an agent could participate in federated learning
schemes:
  - https

paths:
  /getAIModel:
    get:
      tags:
        - AI-model
      summary: Find an AI model for a given context
      description: Find an AI model for a given context by sending the context in json format.
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: query
          name: context
          description: json object
          content:
            application/json: 
              schema:
                type: object
                properties:
                  method:
                    type: string
                  model_usage:
                    type: string
                  agent_id:
                    type: string
                  edge_id:
                    type: string
                  nbr_lanes:
                    type: string
                  seg_type:
                    type: string
                  nbr_exits:
                    type: string
                  radius:
                    type: string
          required: true
      responses:
        '200':
          description: Model Found
        '404':
          description: Not Found
  /pushAIModel/{model}:
    post:
      tags:
        - AI-model
      summary: Sends an AI model to the Edge for a given context
      description: Sends an AI model (binary format) to the Edge for a given context by sending the context in json format.
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: query
          name: context
          description: json object
          content:
            application/json: 
              schema:
                type: object
                properties:
                  method:
                    type: string
                  model_usage:
                    type: string
                  agent_id:
                    type: string
                  edge_id:
                    type: string
                  nbr_lanes:
                    type: string
                  seg_type:
                    type: string
                  nbr_exits:
                    type: string
                  radius:
                    type: string
          required: true
        - name: model
          in: data
          description: the AI model
          required: true
          type: bytes
      responses:
        '200':
          description: successful operation (AI received by Edge)
        '400':
          description: Bad Request 
  /availability:
    post:
      tags:
        - Agents
      summary: announces an agent availability to train for a given context
      description: announces an agent availability to train for a given context
      operationId: findPetsByTags
      produces:
        - application/json
      parameters:
        - in: query
          name: context
          description: json object
          content:
            application/json: 
              schema:
                type: object
                properties:
                  method:
                    type: string
                  model_usage:
                    type: string
                  agent_id:
                    type: string
                  edge_id:
                    type: string
                  nbr_lanes:
                    type: string
                  seg_type:
                    type: string
                  nbr_exits:
                    type: string
                  radius:
                    type: string
          required: true
      responses:
        '200':
          description: successful operation
        '400':
          description: Bad Request 
  /removeAvailability/{agentId}:
    delete:
      tags:
        - Agents
      summary: Remove agent availability
      description: Remove agent availability
      produces:
        - application/xml
        - application/json
      parameters:
        - name: agentId
          in: query
          description: agent ID
          required: true
          type: string
      responses:
        '200':
          description: successful operation
        '400':
          description: Bad Request 
  /trainAIModel:
    get:
      tags:
        - Federated Learning
      summary: Retrieves an AI model for a given context, then participates in the federated training process, then gets the merged AI model  
      description: Retrieves a trained AI model for a given context
      consumes:
        - application/json
      produces:
        - application/xml
      parameters:
        - in: query
          name: context
          description: json object
          content:
            application/json: 
              schema:
                type: object
                properties:
                  method:
                    type: string
                  model_usage:
                    type: string
                  agent_id:
                    type: string
                  edge_id:
                    type: string
                  nbr_lanes:
                    type: string
                  seg_type:
                    type: string
                  nbr_exits:
                    type: string
                  radius:
                    type: string
          required: true
      responses:
        '200':
          description: Model Found
        '404':
          description: Not Found


definitions:
  Context:
    type: object
    properties:
      method:
        type: string
        description: Order Status
        enum:
          - sub-model
          - pub-model
          - push-model
      model_usage:
        type: string
        description: Order Status
        enum:
          - use
          - train
          - available
          - push
      agent_id:
        type: string
        description: ID of Agent
      edge_id:
        type: string
        description: UD of Edge
      nbr_lanes:
        type: string
        description: number of lanes in road segement
        format: int32
      seg_type:
        type: string
        description: type of segement
      nbr_exits:
        type: string
        description: the number of exits in the selected intersection
        format: int32
      radius:
        type: string
        description: the raduis (width) of the selected intersection
        format: int32
        
  ApiResponse:
    type: object
    properties:
      code:
        type: integer
        format: int32
      type:
        type: string
      message:
        type: string
externalDocs:
  description: Find out more about Swagger
  url: http://swagger.io

</code></pre>
</details>
</p>


## 3. Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## 4. License
This work is released under Apache 2.0 license. See the [license file](LICENSE.txt) for more details. Contribution and integrations are appreciated.


