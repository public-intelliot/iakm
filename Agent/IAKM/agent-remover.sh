#!/bin/bash

grep -v '^#' agent.env
export $(grep -v '^#' agent.env | xargs)

docker container stop ${AGENT_HOST} 
docker container rm ${AGENT_HOST} 
docker image rm ${AGENT_HOST} 



