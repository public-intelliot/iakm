#!/bin/bash
grep -v '^#' agent.env
export $(grep -v '^#' agent.env | xargs)

sudo docker login iotplatform.eurecom.fr:5000

sudo docker image pull iotplatform.eurecom.fr:5000/intellio/agent-iakm:latest

sudo docker run --name ${AGENT_HOST}  --net="host" --env-file agent.env iotplatform.eurecom.fr:5000/intellio/agent-iakm:latest
